<div>
    <img width="64" src="images/javascript.png">
</div>

# Music player in vanilla javascript

Enjoy Listening to the world most famous music playlist

### Preview

<div>
    <img width="450" src="images/music-player-preview.png">
</div>