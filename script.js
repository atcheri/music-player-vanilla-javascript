const musicContainer = document.querySelector(".music-container");
const playBtn = document.getElementById("play");
const prevBtn = document.getElementById("prev");
const nextBtn = document.getElementById("next");
const audio = document.getElementById("audio");
const progress = document.querySelector(".progress");
const progressContainer = document.querySelector(".progress-container");
const title = document.getElementById("title");
const cover = document.getElementById("cover");
const songIndexInput = document.getElementById("song-index");
const time = document.getElementById("time");

const songs = [
  {
    name: "Idiocracy",
    image: "Idiocracy.BMP",
    mp3: "Idiocracy.mp3",
  },
  {
    name: "Kodo",
    image: "Kodo.jpg",
    mp3: "Kodo.mp3",
  },
  {
    name: "Mononoke",
    image: "Mononoke.jpg",
    mp3: "Mononoke.mp3",
  },
  {
    name: "Uprising",
    image: "The-Resistance.jpg",
    mp3: "Uprising.mp3",
  },
];

const songIndex = 3;
songIndexInput.value = songIndex;

const initialSong = songs[songIndex];

/**
 *
 * @param {image: string, mp3: string} song
 */
const loadSong = (song) => {
  title.textContent = song.name;
  audio.src = `music/${song.mp3}`;
  cover.src = `images/${song.image}`;
};

const playSong = () => {
  musicContainer.classList.add("play");
  const playIcon = playBtn.querySelector("i.fas");
  playIcon.classList.remove("fa-play");
  playIcon.classList.add("fa-pause");
  audio.play();
};

const pauseSong = () => {
  musicContainer.classList.remove("play");
  const playIcon = playBtn.querySelector("i.fas");
  playIcon.classList.remove("fa-pause");
  playIcon.classList.add("fa-play");
  audio.pause();
};

const loadOtherSong = (increment) => {
  if (increment === 0) {
    return;
  }
  const currIndex = songIndexInput.value;
  const l = songs.length;
  const nextSongIndex = (parseInt(currIndex, 10) + increment + l) % l;
  songIndexInput.value = nextSongIndex;
  loadSong(songs[nextSongIndex]);
  playSong();
};

const prevSong = () => {
  loadOtherSong(-1);
};

const nextSong = () => {
  loadOtherSong(1);
};

const updateProgress = ({ srcElement: { currentTime, duration } }) => {
  if (isNaN(duration)) {
    time.innerText = "loading";
    return;
  }
  const ratio = (currentTime / duration) * 100;
  progress.style.width = `${ratio}%`;
  const currMoment = moment(currentTime, "X");
  const durationMoment = moment(duration, "X");
  time.innerText = `${currMoment.format("mm:ss")} / ${durationMoment.format(
    "mm:ss"
  )}`;
};

const setProgress = (e) => {
  const max = progressContainer.clientWidth;
  const clickX = e.offsetX;
  const ratio = clickX / max;
  const { duration } = audio;
  audio.currentTime = duration * ratio;
};

loadSong(initialSong);

playBtn.addEventListener("click", () => {
  const isPlaying = musicContainer.classList.contains("play");
  if (isPlaying) {
    pauseSong();
    return;
  }
  playSong();
});

prevBtn.addEventListener("click", prevSong);
nextBtn.addEventListener("click", nextSong);
audio.addEventListener("timeupdate", updateProgress);
audio.addEventListener("ended", nextSong);
progressContainer.addEventListener("click", setProgress);
